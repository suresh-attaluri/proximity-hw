package dataload

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"net/http"
)

func loadSubjects() {
	apiUrl := "http://localhost:8080/subject"

	subjects := make(map[string]string)

	subjects["maths"] = "Maths"
	subjects["science"] = "Science"
	subjects["chemistry"] = "Chemistry"
	subjects["physics"] = "Physics"

	for code, name := range subjects {
		fmt.Printf("loading subject %s", name)
		postBody, _ := json.Marshal(map[string]string{
			"name": name,
			"code": code,
		})

		responseBody := bytes.NewBuffer(postBody)
		_, err := http.Post(apiUrl, "application/json", responseBody)

		if err != nil {
			fmt.Print(err.Error())
		}
	}
}

func loadCourses() {
	apiUrl := "http://localhost:8080/course"
	courses := []*models.CreateCourseReq{};
	courses = append(courses, &models.CreateCourseReq{
		Code:     "it",
		Name:     "information technology",
		Subjects: []int64{1, 2},
	})

	courses = append(courses, &models.CreateCourseReq{
		Code:     "cse",
		Name:     "Computer Science",
		Subjects: []int64{1, 3},
	})

	courses = append(courses, &models.CreateCourseReq{
		Code:     "ece",
		Name:     "Electronics",
		Subjects: []int64{2, 4},
	})

	for _, course := range courses {
		fmt.Printf("loading course %s", course.Name)
		postBody, _ := json.Marshal(course)
		responseBody := bytes.NewBuffer(postBody)
		_, err := http.Post(apiUrl, "application/json", responseBody)

		if err != nil {
			fmt.Print(err.Error())
		}
	}
}

func loadVideos() {
	apiUrl := "http://localhost:8080/video"
	videos := []*models.UploadContentReq{}

	videos = append(videos, &models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			Code: "maths",
			Name: "Maths",
		}, {
			Code: "statistics",
			Name: "Statistics",
		}},
		Title: "statistics video 1",
	})

	videos = append(videos, &models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			Code: "chemistry",
			Name: "Chemistry",
		}, {
			Code: "atomicstructure",
			Name: "Atomic Structure",
		}},
		Title: "Atomic Structure video 2",
	})

	videos = append(videos, &models.UploadContentReq{
		Code:        "vd-3",
		Link:        "http://www.video-link/vd-3",
		SubjectCode: "physics",
		Tags: []*models.Tag{{
			Code: "physics",
			Name: "Physics",
		}, {
			Code: "atomicstructure",
			Name: "Thermo Dynamics",
		}},
		Title: "Thermo Dynamics video 3",
	})

	for _, video := range videos {
		postBody, _ := json.Marshal(video)
		responseBody := bytes.NewBuffer(postBody)
		_, err := http.Post(apiUrl, "application/json", responseBody)

		if err != nil {
			fmt.Print(err.Error())
		}
	}
}

func loadWebinar() {
	apiUrl := "http://localhost:8080/webinar"
	videos := []*models.UploadContentReq{}

	videos = append(videos, &models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	videos = append(videos, &models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	videos = append(videos, &models.UploadContentReq{
		Code:        "wb-3",
		Link:        "http://www.webinar-link/wb-3",
		SubjectCode: "physics",
		Tags: []*models.Tag{{
			ID: 5,
		}, {
			ID: 6,
		}},
		Title: "Thermo Dynamics webinar 3",
	})

	for _, video := range videos {
		postBody, _ := json.Marshal(video)
		responseBody := bytes.NewBuffer(postBody)
		_, err := http.Post(apiUrl, "application/json", responseBody)

		if err != nil {
			fmt.Print(err.Error())
		}
	}
}

func RunDataload() {
	loadSubjects()
	loadCourses()
	loadVideos()
	loadWebinar()
}
