# Proximity HW

# Domain Model

![domain-model](docs/domain-model.jpg)

Domain model has three bounded context:

1. content: 
   
    to upload videos/webinars or modify title and subjects of the content.
   * Entities
     
        * ```content```

          we have following two value objects for content.
          
          ```subject``` - subject belong to the  bounded context ```course```.
          since this is an out of bounded context reference we have subject
          code as value onject to link.
          
          ```tags``` tags belong to the same bounded context and content value
          object will be referenced by id         
     
        * ```tag```
2. user:
   
   to manage user accounts and assign roles.
   
3. course:
   
   to create and modify courses and subjects.
   * Entities
     
        * ```course```
          
          ```subjects``` value object reference by id as both belong to 
          same bounded context
          
        * ```subject```
    
# Data Model

![data-model](docs/data-model.jpg)

Data Model would be split into three databases one for each bounded context 
of the domain model

1. content:
    * Tables
        * ```content``` to store videos and webinars
        * ```tag``` to store tags
        * ```content_tags``` composite table to link both tags and content
    
2. user:
    * Tables 
        * ```user```: to store users
    
3. course:
    * Tables:
        * ```course```: to store courses
        * ```subjet```: to store subjects
        * ```course_subjects```: composite table to link course and subjects
    
# API Implementation

## Specification
we used ```OpenAPI``` to define specification of the rest api and used ```go-swagger``` 
to serialize/deserialize the specification. please refer the [swagger.yml](swagger.yml) / [swagger-ui](https://proximity-hw-docs.dev.lbnapps.net/docs) for the spec

## Persistence
to speed up dev we are saving the data in memory. this could be 
further improved by connecting this to any sql database.

## Sorting
we have implemented sorting using heap sort algorithm to get most viewed content.

we choose heap sort for its low time and space complexity. 

    considering we have array of n elements and t being the number of top records 

    step 1: we build heap from the array with time complexity O(n log n) as traversing through the heap to last node 
    would take O(log n) time complexity, building a heap with n elements would take O(n log n).

    step 2: swap root node with last node and remove the root node from the heap and save it to result array.

    step 3: repeat step 1 and 2 for t times. since we have to repeat this task for t times the overall time 
    complexity would be O(n*t log n).

please refer the heap implementation [heap](ds/heap.go)

## Searching
we have implemented searching videos/webinars by keywords of the title using trie data structure

api endpoint: ```/video/title/{keywords}```. keywords are list of sub strings that we 
try to find in the title.

    step 1: build trie for the given keywords. time complexity of trie for one word would be O(s) where s is the 
    length of the string. doing this for k strings would be O(ks)

    step 2: for each character in the title see if we have child node in the trie:
                time complexity O(t) t being the length of the title
            if found then look for next character in title
                time complexity O(s) s being the max length of the keywords
            the time complexity to look for the keywords in a title would be O(ts) and to do this for n titles it 
            would take O(nts)

    the total time complexity would be O(ks + nts)
    space complexity would be O(ks) as it would k keywords of s max length to store as trie

plase refer the trie implementation [trie](ds/trie.go)

## Testing
we have used testing package to write test cases for both ```content``` and ```course```
do refer the test cases implementation from here [content_test](handlers/content_test.go)
and [course_test](handlers/course_test.go)

## Dataload
we have included a api endpoint to load sample data to the application at /dataload. 
this endpoint loads content/tags/course and subjects to the application

# Ops
## Build & Deployment
* The application has been dockerized to be cloudnative
* The application is deployed on to one of my test kuberenetes clusters using Helm
* please refer the link to access the api [API Link](https://proximity-hw.dev.lbnapps.net/)

# Improvements

1. we can use messaging system to asynchronously processing the requests.
2. we can split this to three services based on bounded context

