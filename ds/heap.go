package ds

import (
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
)

type MostViewedContent []*models.Content

func NewMostViewedContent(contents []*models.Content) *MostViewedContent {
	heap := MostViewedContent(contents)
	ptr := &heap
	ptr.buildHeap(contents)
	return ptr
}

func (h *MostViewedContent) GetContentList() []*models.Content {
	return *h
}

func (h *MostViewedContent) buildHeap(contents []*models.Content) {
	first := (len(contents) - 2) / 2
	for currentIndex := first + 1; currentIndex >= 0; currentIndex-- {
		h.shiftDown(currentIndex, len(contents)-1)
	}
}

func (h *MostViewedContent) shiftDown(currentIndex, endIndex int) {
	childOneIdx := currentIndex*2 + 1
	for childOneIdx <= endIndex {
		childTwoIdx := -1
		if currentIndex*2+2 <= endIndex {
			childTwoIdx = currentIndex*2 + 2
		}
		indexToSwap := childOneIdx
		if childTwoIdx > -1 && (*h)[childTwoIdx].Views > (*h)[childOneIdx].Views {
			indexToSwap = childTwoIdx
		}
		if (*h)[indexToSwap].Views > (*h)[currentIndex].Views {
			h.swap(currentIndex, indexToSwap)
			currentIndex = indexToSwap
			childOneIdx = currentIndex*2 + 1
		} else {
			return
		}
	}
}

func (h MostViewedContent) swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h MostViewedContent) Length() int {
	return len(h)
}

func (h *MostViewedContent) Remove() *models.Content {
	l := h.Length()
	h.swap(0, l-1)
	peeked := (*h)[l-1]
	*h = (*h)[0 : l-1]
	h.shiftDown(0, l-2)
	return peeked
}
