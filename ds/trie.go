package ds

type Trie struct {
	Children map[byte]Trie
	Word     string
}

func (t Trie) Add(word string) {
	current := t
	for i := range word {
		letter := word[i]
		if _, found := current.Children[letter]; !found {
			current.Children[letter] = Trie{
				Children: map[byte]Trie{},
			}
		}
		current = current.Children[letter]
	}
	current.Children['*'] = Trie{
		Children: map[byte]Trie{},
		Word:     word,
	}
}

func (t Trie) ContainsIn(str string) bool {
	for i := range str {
		current := t
		for j := i; j < len(str); j++ {
			currentChar := str[j]
			if _, found := current.Children[currentChar]; !found {
				break
			}
			current = current.Children[currentChar]
			if _, found := current.Children['*']; found {
				return true
			}
		}
	}
	return false
}
