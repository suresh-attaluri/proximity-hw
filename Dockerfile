FROM golang:1.14
COPY app /app
EXPOSE 8080
CMD ["/app"]
