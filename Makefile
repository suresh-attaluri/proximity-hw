port ?= 8080
name = "proximity-hw"
repo_root ?= .
v ?= v0.0.1
install_swagger:
	brew tap go-swagger/go-swagger
	brew install go-swagger
deps:
	go mod download
fmt:
	go fmt gitlab.com/suresh-attaluri/proximity-hw
build: deps fmt
	env GOOS=linux GOARCH=amd64 go build -o app
build_darwin: deps fmt
	env GOOS=darwin GOARCH=amd64 go build -o app
run:
	go run *.go
test:
	go test ./...
run_docker: image
	docker rm -f ${name} | true
	docker run --name ${name} -p ${port}:8080 ${name}
swagger:
	rm -rf zzgen;mkdir zzgen
	swagger generate server -t zzgen --exclude-main -A Proximity
swagger-ui:
	swagger serve swagger.yml --port 9081 --no-open --flavor swagger
