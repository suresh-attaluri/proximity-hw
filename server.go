package main

import (
	"github.com/go-openapi/loads"
	"github.com/rs/zerolog"
	l "github.com/rs/zerolog/log"
	"gitlab.com/suresh-attaluri/proximity-hw/handlers"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
	"log"
)

func setupLogger() {
	debug := false
	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		l.Logger = l.With().Caller().Logger()
	}
	l.Logger = l.With().Str("service", "proximity").Logger()
}

func main() {
	const defaultPort = 8080

	setupLogger()

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")

	if err != nil {
		l.Fatal().Msgf("failed to parse swagger spec file")
	}

	h := &handlers.Handlers{}

	api := operations.NewProximityAPI(swaggerSpec)
	api.RootHandler = operations.RootHandlerFunc(h.RootHandler)
	api.HealthHandler = operations.HealthHandlerFunc(h.HealthHandler)
	api.DataloadHandler = operations.DataloadHandlerFunc(h.DataloadHandler)

	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	api.CreateCourseHandler = operations.CreateCourseHandlerFunc(h.CreateCourseHandler)
	api.CreateSubjectHandler = operations.CreateSubjectHandlerFunc(h.CreateSubjectHandler)
	api.CreateTagHandler = operations.CreateTagHandlerFunc(h.CreateTagHandler)

	api.GetAllContentsHandler = operations.GetAllContentsHandlerFunc(h.GetAllContentsHandler)
	api.GetAllWebinarsHandler = operations.GetAllWebinarsHandlerFunc(h.GetAllWebinarsHandler)
	api.GetAllVideosHandler = operations.GetAllVideosHandlerFunc(h.GetAllVideosHandler)
	api.GetAllCoursesHandler = operations.GetAllCoursesHandlerFunc(h.GetAllCoursesHandler)
	api.GetAllSubjectsHandler = operations.GetAllSubjectsHandlerFunc(h.GetAllSubjectsHandler)
	api.GetAllTagsHandler = operations.GetAllTagsHandlerFunc(h.GetAllTagsHandler)

	api.DeleteSubjectHandler = operations.DeleteSubjectHandlerFunc(h.DeleteSubjectHandler)
	api.DeleteCourseHandler = operations.DeleteCourseHandlerFunc(h.DeleteCourseHandler)
	api.DeleteVideoHandler = operations.DeleteVideoHandlerFunc(h.DeleteVideoHandler)
	api.DeleteWebinarHandler = operations.DeleteWebinarHandlerFunc(h.DeleteWebinarHandler)
	api.DeleteTagHandler = operations.DeleteTagHandlerFunc(h.DeleteTagHandler)

	api.UpdateSubjectHandler = operations.UpdateSubjectHandlerFunc(h.UpdateSubjectHandler)
	api.UpdateCourseHandler = operations.UpdateCourseHandlerFunc(h.UpdateCourseHandler)
	api.UpdateVideoHandler = operations.UpdateVideoHandlerFunc(h.UpdateVideoHandler)
	api.UpdateWebinarHandler = operations.UpdateWebinarHandlerFunc(h.UpdateWebinarHandler)
	api.UpdateTagHandler = operations.UpdateTagHandlerFunc(h.UpdateTagHandler)

	api.GetVideoByIDHandler = operations.GetVideoByIDHandlerFunc(h.GetVideoByIDHandler)
	api.GetWebinarByIDHandler = operations.GetWebinarByIDHandlerFunc(h.GetWebinarByIDHandler)

	api.GetContentsByCourseNameHandler = operations.GetContentsByCourseNameHandlerFunc(h.GetContentsByCourseNameHandler)
	api.GetContentsBySubjectNameHandler = operations.GetContentsBySubjectNameHandlerFunc(h.GetContentsBySubjectNameHandler)
	api.GetContentsByTagNameHandler = operations.GetContentsByTagNameHandlerFunc(h.GetContentsByTagNameHandler)

	api.GetSubjectByNameHandler = operations.GetSubjectByNameHandlerFunc(h.GetSubjectByNameHandler)
	api.GetSubjectsByCourseNameHandler = operations.GetSubjectsByCourseNameHandlerFunc(h.GetSubjectsByCourseNameHandler)

	api.GetMostViewedVideosHandler = operations.GetMostViewedVideosHandlerFunc(h.GetMostViewedVideosHandler)

	api.GetVideosByTitleHandler = operations.GetVideosByTitleHandlerFunc(h.GetVideosByTitleHandler)
	api.GetWebinarsByTitleHandler = operations.GetWebinarsByTitleHandlerFunc(h.GetWebinarsByTitleHandler)

	api.GetCoursesBySubjectCodeHandler = operations.GetCoursesBySubjectCodeHandlerFunc(h.GetCoursesBySubjectCodeHandler)
	api.GetWebinarsBySubjectCodeHandler = operations.GetWebinarsBySubjectCodeHandlerFunc(h.GetWebinarsBySubjectCodeHandler)

	server := restapi.NewServer(api)
	defer server.Shutdown()
	server.Port = defaultPort
	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
