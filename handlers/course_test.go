package handlers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCreateCourseHandler_GivenNewCourseParams_NewCourseToBeAddedtoCourseList(t *testing.T) {
	h := &Handlers{}
	courses.Data = []*models.Course{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateCourseHandler = operations.CreateCourseHandlerFunc(h.CreateCourseHandler)
	handler, err := GetAPIHandler(api)

	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	course, _ := json.Marshal(&models.CreateCourseReq{
		Code:     "c-1",
		Name:     "course 1",
		Subjects: []int64{1, 2},
	})

	http.Post(ts.URL+"/course", "application/json", bytes.NewBuffer(course))

	if courses.Data[0].Code != "c-1" {
		t.Error("course not loaded")
	}
}

func TestCreateSubjectHandler_GivenNewSubjectParams_NewSubjectToBeAddedtoList(t *testing.T) {
	h := &Handlers{}
	subjects.Data = []*models.Subject{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateSubjectHandler = operations.CreateSubjectHandlerFunc(h.CreateSubjectHandler)
	handler, err := GetAPIHandler(api)

	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	subject, _ := json.Marshal(&models.CreateSubjectReq{
		Code: "s-1",
		Name: "subject 1",
	})

	http.Post(ts.URL+"/subject", "application/json", bytes.NewBuffer(subject))

	if subjects.Data[0].Code != "s-1" {
		t.Error("subject not loaded")
	}
}
