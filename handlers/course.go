package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	l "github.com/rs/zerolog/log"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
	"sort"
)

var courses = models.Courses{
	Data:      []*models.Course{},
	RunningID: 0,
}

var subjects = models.Subjects{
	Data:      []*models.Subject{},
	RunningID: 0,
}

func (h *Handlers) CreateCourseHandler(params operations.CreateCourseParams) middleware.Responder {
	l.Info().Msgf("processing create course req %v", params.Body.Code)
	c := &models.Course{
		ID:       courses.RunningID + 1,
		Code:     params.Body.Code,
		Name:     params.Body.Name,
		Status:   "active",
		Subjects: params.Body.Subjects,
	}

	courses.Data = append(courses.Data, c)
	courses.RunningID = courses.RunningID + 1
	return operations.NewCreateCourseOK().WithPayload(c)
}

func (h *Handlers) CreateSubjectHandler(params operations.CreateSubjectParams) middleware.Responder {
	l.Info().Msgf("processing create subject req %v", params.Body.Code)
	c := &models.Subject{
		ID:     subjects.RunningID + 1,
		Code:   params.Body.Code,
		Name:   params.Body.Name,
		Status: "active",
	}
	subjects.Data = append(subjects.Data, c)
	subjects.RunningID = subjects.RunningID + 1
	return operations.NewCreateSubjectOK().WithPayload(c)
}

func (h *Handlers) CreateTagHandler(params operations.CreateTagParams) middleware.Responder {
	l.Info().Msgf("processing create tag req %v", params.Body.Code)
	c := &models.Tag{
		ID:     tags.RunningID + 1,
		Code:   params.Body.Code,
		Name:   params.Body.Name,
		Status: "active",
	}
	tags.Data = append(tags.Data, c)
	tags.RunningID = tags.RunningID + 1
	return operations.NewCreateTagOK().WithPayload(c)
}

func (h *Handlers) GetAllCoursesHandler(params operations.GetAllCoursesParams) middleware.Responder {
	l.Info().Msgf("processing get all courses req")
	c := &models.Courses{
		Data:      []*models.Course{},
		RunningID: courses.RunningID,
	}
	for _, course := range courses.Data {
		if course.Status == "active" {
			c.Data = append(c.Data, course)
		}
	}
	return operations.NewGetAllCoursesOK().WithPayload(c)
}

func (h *Handlers) GetAllSubjectsHandler(params operations.GetAllSubjectsParams) middleware.Responder {
	l.Info().Msgf("processing get all subjects req")
	s := &models.Subjects{
		Data:      []*models.Subject{},
		RunningID: subjects.RunningID,
	}
	for _, subject := range subjects.Data {
		if subject.Status == "active" {
			s.Data = append(s.Data, subject)
		}
	}
	return operations.NewGetAllSubjectsOK().WithPayload(s)
}

func (h *Handlers) GetAllTagsHandler(params operations.GetAllTagsParams) middleware.Responder {
	l.Info().Msgf("processing get all tags req")
	s := &models.Tags{
		Data:      []*models.Tag{},
		RunningID: tags.RunningID,
	}
	for _, tag := range tags.Data {
		if tag.Status == "active" {
			s.Data = append(s.Data, tag)
		}
	}
	return operations.NewGetAllTagsOK().WithPayload(s)
}

func (h *Handlers) DeleteCourseHandler(params operations.DeleteCourseParams) middleware.Responder {
	l.Info().Msgf("processing delete course req for id %v", params.ID)
	foundIndex := 0
	length := len(courses.Data)
	res := &models.Course{}
	for _, w := range courses.Data {
		if w.ID == params.ID {
			w.Status = "delete"
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("course does not exist %v", params.ID)
	}
	return operations.NewDeleteCourseOK().WithPayload(res)
}

func (h *Handlers) DeleteSubjectHandler(params operations.DeleteSubjectParams) middleware.Responder {
	l.Info().Msgf("processing delete subject req for id %v", params.ID)
	foundIndex := 0
	length := len(subjects.Data)
	res := &models.Subject{}
	for _, w := range subjects.Data {
		if w.ID == params.ID {
			w.Status = "delete"
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("subject does not exist %v", params.ID)
	}
	return operations.NewDeleteSubjectOK().WithPayload(res)
}

func (h *Handlers) DeleteTagHandler(params operations.DeleteTagParams) middleware.Responder {
	l.Info().Msgf("processing delete tag req for id %v", params.ID)
	foundIndex := 0
	length := len(tags.Data)
	res := &models.Tag{}
	for _, w := range tags.Data {
		if w.ID == params.ID {
			w.Status = "delete"
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("tag does not exist %v", params.ID)
	}
	return operations.NewDeleteTagOK().WithPayload(res)
}

func (h *Handlers) UpdateCourseHandler(params operations.UpdateCourseParams) middleware.Responder {
	l.Info().Msgf("processing update course req for id %v", params.Body.ID)
	foundIndex := 0
	length := len(courses.Data)
	res := &models.Course{}
	subjectIds := params.Body.Subjects
	sort.Slice(subjectIds, func(i, j int) bool { return subjectIds[i] < subjectIds[j] })
	for _, w := range courses.Data {
		if w.ID == params.Body.ID {
			w.Name = params.Body.Name
			w.Subjects = subjectIds
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("course does not exist %v", params.Body.ID)
	}
	return operations.NewDeleteCourseOK().WithPayload(res)
}

func (h *Handlers) UpdateSubjectHandler(params operations.UpdateSubjectParams) middleware.Responder {
	l.Info().Msgf("processing update subject req for id %v", params.Body.ID)
	foundIndex := 0
	length := len(subjects.Data)
	res := &models.Subject{}
	for _, w := range subjects.Data {
		if w.ID == params.Body.ID {
			w.Name = params.Body.Name
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("subject does not exist %v", params.Body.ID)
	}
	return operations.NewDeleteSubjectOK().WithPayload(res)
}

func (h *Handlers) UpdateTagHandler(params operations.UpdateTagParams) middleware.Responder {
	l.Info().Msgf("processing update tag req for id %v", params.Body.ID)
	foundIndex := 0
	length := len(tags.Data)
	res := &models.Tag{}
	for _, w := range tags.Data {
		if w.ID == params.Body.ID {
			w.Name = params.Body.Name
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("tag does not exist %v", params.Body.ID)
	}
	return operations.NewDeleteTagOK().WithPayload(res)
}

func (h *Handlers) GetSubjectByNameHandler(params operations.GetSubjectByNameParams) middleware.Responder {
	l.Info().Msgf("processing get subject by name req")
	subject := &models.Subject{}
	for _, s := range subjects.Data {
		if s.Name == params.Name {
			subject = s
			break
		}
	}
	return operations.NewGetSubjectByNameOK().WithPayload(subject)
}

func (h *Handlers) GetSubjectsByCourseNameHandler(params operations.GetSubjectsByCourseNameParams) middleware.Responder {
	l.Info().Msgf("processing get subject by course name req")
	subjectIds := []int64{}
	lSubjects := &models.Subjects{
		Data:      []*models.Subject{},
		RunningID: subjects.RunningID,
	}
	for _, c := range courses.Data {
		if c.Name == params.Name {
			subjectIds = append(subjectIds, c.Subjects...)
			break
		}
	}
	if len(subjectIds) > 0 {
		for _, s := range subjects.Data {
			if s.ID == subjectIds[0] {
				lSubjects.Data = append(lSubjects.Data, s)
				if len(subjectIds) > 1 {
					subjectIds = subjectIds[1:]
				} else if len(subjectIds) == 0 {
					break
				}
			}
		}
	}
	return operations.NewGetSubjectsByCourseNameOK().WithPayload(lSubjects)
}

func (h *Handlers) GetCoursesBySubjectCodeHandler(params operations.GetCoursesBySubjectCodeParams) middleware.Responder {
	l.Info().Msgf("processing get courses by subject name req")
	var subjectId int64
	lCourses := &models.Courses{
		Data:      []*models.Course{},
		RunningID: courses.RunningID,
	}
	for _, c := range subjects.Data {
		if c.Code == params.Subjectcode {
			subjectId = c.ID
			break
		}
	}
	for _, c := range courses.Data {
		for _, s := range c.Subjects {
			if s == subjectId {
				lCourses.Data = append(lCourses.Data, c)
			}
		}
	}
	return operations.NewGetCoursesBySubjectCodeOK().WithPayload(lCourses)
}
