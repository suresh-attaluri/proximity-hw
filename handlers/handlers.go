package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/suresh-attaluri/proximity-hw/dataload"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
)

type Handlers struct {
}

func (h *Handlers) RootHandler(params operations.RootParams) middleware.Responder {
	return operations.NewRootOK().WithPayload(&models.Health{Status: "up"})
}

func (h *Handlers) DataloadHandler(params operations.DataloadParams) middleware.Responder {
	dataload.RunDataload()
	return operations.NewRootOK().WithPayload(&models.Health{Status: "up"})
}

func (h *Handlers) HealthHandler(params operations.HealthParams) middleware.Responder {
	return operations.NewHealthOK().WithPayload(&models.Health{Status: "up"})
}


