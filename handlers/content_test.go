package handlers

import (
	"bytes"
	"encoding/json"
	"github.com/go-openapi/loads"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func getAPI() (*operations.ProximityAPI, error) {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		return nil, err
	}
	api := operations.NewProximityAPI(swaggerSpec)
	return api, nil
}

func GetAPIHandler(api *operations.ProximityAPI) (http.Handler, error) {
	s := restapi.NewServer(api)
	s.ConfigureAPI()

	err := api.Validate()
	if err != nil {
		return nil, err
	}
	return s.GetHandler(), nil
}

func TestCreateWebinarHandler_GivenWebinarParams_NewWebinarToBeAddedWebinarsList(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	if webinars.Data[0].Code != "wb-1" {
		t.Error("webinar not loaded")
	}
}

func TestCreateWebinarHandler_GivenTwoWebinarParams_TwoNewWebinarsToBeAddedWebinarsList(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	webinar2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar2))

	if webinars.Data[0].Code != "wb-1" && webinars.Data[1].Code != "wb-2" {
		t.Error("webinars not loaded")
	}
}

func TestCreateVideoHandler_GivenVideoParams_NewVideoToBeAddedVideossList(t *testing.T) {
	h := &Handlers{}
	videos.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	if videos.Data[0].Code != "vd-1" {
		t.Error("video not loaded")
	}
}

func TestCreateVideoHandler_GivenTwoVideoParams_TwoNewVideosToBeAddedVideosList(t *testing.T) {
	h := &Handlers{}
	videos.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	video2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure video 2",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video2))

	if videos.Data[0].Code != "vd-1" && videos.Data[1].Code != "vd-2" {
		t.Error("videos not loaded")
	}
}

func TestGetAllVideosHandler_GivenThereAreTwoVideosAvailable_ReturnTwoVideos(t *testing.T) {
	h := &Handlers{}
	videos.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.GetAllVideosHandler = operations.GetAllVideosHandlerFunc(h.GetAllVideosHandler)
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	video2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure video 2",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video2))

	res, _ := http.Get(ts.URL + "/video")

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	lContents := &models.Contents{
		Data:      nil,
		RunningID: 0,
	}

	bodyBytes, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(bodyBytes, lContents)
	if len(lContents.Data) != 2 {
		t.Errorf("not all videos were returned in the reponse expected: %v actual: %v", 2, len(lContents.Data))
	}
}

func TestGetAllWebinarsHandler_GivenThereAreTwoWebinarsAvailable_ReturnTwoWebinars(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.GetAllWebinarsHandler = operations.GetAllWebinarsHandlerFunc(h.GetAllWebinarsHandler)
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics webinar 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	webinar2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar2))

	res, _ := http.Get(ts.URL + "/webinar")

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	lContents := &models.Contents{
		Data:      nil,
		RunningID: 0,
	}

	bodyBytes, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(bodyBytes, lContents)
	if len(lContents.Data) != 2 {
		t.Errorf("not all webinars were returned in the reponse expected: %v actual: %v", 2, len(lContents.Data))
	}
}

func TestGetAllContentsHandler_GivenThereAreTwoWebinarsAndTwoVideosAvailable_ReturnAll(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	videos.Data = []*models.Content{}
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)
	api.GetAllContentsHandler = operations.GetAllContentsHandlerFunc(h.GetAllContentsHandler)
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics webinar 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	webinar2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar2))

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	video2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure video 2",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video2))

	res, _ := http.Get(ts.URL + "/content")

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	lContents := &models.Contents{
		Data:      nil,
		RunningID: 0,
	}

	bodyBytes, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(bodyBytes, lContents)

	if len(lContents.Data) != 4 {
		t.Errorf("not all webinars were returned in the reponse expected: %v actual: %v", 4, len(lContents.Data))
	}
}

func TestDeleteWebinarHandler_GivenThereAreTwoWebinarsAvailable_DeleteOneWebinar(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	webinars.RunningID = 0
	api, err := getAPI()
	if err != nil {
		t.Fatalf("error in creating api")
	}
	api.DeleteWebinarHandler = operations.DeleteWebinarHandlerFunc(h.DeleteWebinarHandler)
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)

	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatalf("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics webinar 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	webinar2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar2))

	req, _ := http.NewRequest(http.MethodDelete, ts.URL+"/webinar?id=1", nil)
	client := http.Client{}
	res, _ := client.Do(req)

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if webinars.Data[0].Status != "delete" {
		t.Error("webinar not deleted")
	}
}

func TestDeleteVideoHandler_GivenThereAreTwoVideosAvailable_DeleteOneVideo(t *testing.T) {
	h := &Handlers{}
	videos.Data = []*models.Content{}
	videos.RunningID = 0
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.DeleteVideoHandler = operations.DeleteVideoHandlerFunc(h.DeleteVideoHandler)
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	video2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure video 2",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video2))

	req, _ := http.NewRequest(http.MethodDelete, ts.URL+"/video?id=1", nil)
	client := http.Client{}
	res, _ := client.Do(req)

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if videos.Data[0].Status != "delete" {
		t.Error("video not deleted")
	}
}

func TestUpdateWebinarHandler_GivenThereAreTwoWebinarsAvailable_UpdateOneWebinar(t *testing.T) {
	h := &Handlers{}
	webinars.Data = []*models.Content{}
	webinars.RunningID = 0
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.UpdateWebinarHandler = operations.UpdateWebinarHandlerFunc(h.UpdateWebinarHandler)
	api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(h.CreateWebinarHandler)

	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	webinar1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-1",
		Link:        "http://www.webinar-link/wb-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics webinar 1",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar1))

	webinar2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "wb-2",
		Link:        "http://www.webinar-link/wb-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure webinar 2",
	})

	http.Post(ts.URL+"/webinar", "application/json", bytes.NewBuffer(webinar2))

	webinar3, _ := json.Marshal(&models.UpdateContentReq{
		ID:    1,
		Link:  "http://www.webinar-link/wb-3",
		Title: "statistics webinar 3",
	})

	req, _ := http.NewRequest(http.MethodPut, ts.URL+"/webinar", bytes.NewBuffer(webinar3))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	client := http.Client{}
	res, _ := client.Do(req)

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if webinars.Data[0].Link != "http://www.webinar-link/wb-3" {
		t.Error("webinar link not updated")
	}
	if webinars.Data[0].Title != "statistics webinar 3" {
		t.Error("webinar title not updated")
	}
}

func TestUpdateVideoHandler_GivenThereAreTwoVideosAvailable_UpdateOneVideo(t *testing.T) {
	h := &Handlers{}
	videos.Data = []*models.Content{}
	videos.RunningID = 0
	api, err := getAPI()
	if err != nil {
		t.Fatal("error in creating api")
	}
	api.UpdateVideoHandler = operations.UpdateVideoHandlerFunc(h.UpdateVideoHandler)
	api.CreateVideoHandler = operations.CreateVideoHandlerFunc(h.CreateVideoHandler)
	handler, err := GetAPIHandler(api)
	if err != nil {
		t.Fatal("error in creating api handler")
	}

	ts := httptest.NewServer(handler)

	video1, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-1",
		Link:        "http://www.video-link/vd-1",
		SubjectCode: "maths",
		Tags: []*models.Tag{{
			ID: 1,
		}, {
			ID: 2,
		}},
		Title: "statistics video 1",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video1))

	video2, _ := json.Marshal(&models.UploadContentReq{
		Code:        "vd-2",
		Link:        "http://www.video-link/vd-2",
		SubjectCode: "chemistry",
		Tags: []*models.Tag{{
			ID: 3,
		}, {
			ID: 4,
		}},
		Title: "Atomic Structure video 2",
	})

	http.Post(ts.URL+"/video", "application/json", bytes.NewBuffer(video2))

	video3, _ := json.Marshal(&models.UpdateContentReq{
		ID:    1,
		Link:  "http://www.video-link/vd-3",
		Title: "statistics video 3",
	})

	req, _ := http.NewRequest(http.MethodPut, ts.URL+"/video", bytes.NewBuffer(video3))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	client := http.Client{}
	res, _ := client.Do(req)

	if status := res.StatusCode; status != 200 {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if videos.Data[0].Link != "http://www.video-link/vd-3" {
		t.Error("video link not updated")
	}
	if videos.Data[0].Title != "statistics video 3" {
		t.Error("video title not updated")
	}
}
