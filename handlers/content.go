package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/go-openapi/runtime/middleware"
	l "github.com/rs/zerolog/log"
	"gitlab.com/suresh-attaluri/proximity-hw/ds"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
	"io/ioutil"
	"net/http"
	"sync"
)

var videos = models.Contents{
	Data:      []*models.Content{},
	RunningID: 0,
}

var webinars = models.Contents{
	Data:      []*models.Content{},
	RunningID: 0,
}

var tags = models.Tags{
	Data:      []*models.Tag{},
	RunningID: 0,
}

func (h *Handlers) CreateWebinarHandler(params operations.CreateWebinarParams) middleware.Responder {
	l.Info().Msgf("processing upload webinar req %v", params.Body.Code)
	webinarTags := []int64{}
	for _, t := range params.Body.Tags {
		if t.ID == 0 {
			c := &models.Tag{
				ID:     tags.RunningID + 1,
				Code:   t.Code,
				Name:   t.Name,
				Status: "active",
			}
			tags.Data = append(tags.Data, c)
			tags.RunningID = tags.RunningID + 1
			t.ID = tags.RunningID
		}
		webinarTags = append(webinarTags, t.ID)
	}

	c := &models.Content{
		ID:          webinars.RunningID + 1,
		ContentType: "webinar",
		Code:        params.Body.Code,
		Link:        params.Body.Link,
		Title:       params.Body.Title,
		Status:      "active",
		Tags:        webinarTags,
		SubjectCode: params.Body.SubjectCode,
		Views:       0,
	}
	data := append(webinars.Data, c)
	webinars.RunningID = webinars.RunningID + 1
	webinars.Data = data
	return operations.NewCreateWebinarOK().WithPayload(c)
}

func (h *Handlers) CreateVideoHandler(params operations.CreateVideoParams) middleware.Responder {
	l.Info().Msgf("processing upload video req %v", params.Body.Code)
	videoTags := []int64{}
	for _, t := range params.Body.Tags {
		if t.ID == 0 {
			c := &models.Tag{
				ID:     tags.RunningID + 1,
				Code:   t.Code,
				Name:   t.Name,
				Status: "active",
			}
			tags.Data = append(tags.Data, c)
			tags.RunningID = tags.RunningID + 1
			t.ID = tags.RunningID
		}
		videoTags = append(videoTags, t.ID)
	}
	c := &models.Content{
		ID:          videos.RunningID + 1,
		ContentType: "video",
		Code:        params.Body.Code,
		Link:        params.Body.Link,
		Title:       params.Body.Title,
		Status:      "active",
		Tags:        videoTags,
		SubjectCode: params.Body.SubjectCode,
		Views:       0,
	}
	videos.Data = append(videos.Data, c)
	videos.RunningID = videos.RunningID + 1
	return operations.NewCreateVideoOK().WithPayload(c)
}

func (h *Handlers) GetAllVideosHandler(params operations.GetAllVideosParams) middleware.Responder {
	l.Info().Msgf("processing get all videos req")
	contents := &models.Contents{
		Data:      []*models.Content{},
		RunningID: videos.RunningID,
	}
	for _, c := range videos.Data {
		if c.Status == "active" {
			contents.Data = append(contents.Data, c)
		}
	}
	return operations.NewGetAllVideosOK().WithPayload(contents)
}

func (h *Handlers) GetAllWebinarsHandler(params operations.GetAllWebinarsParams) middleware.Responder {
	l.Info().Msgf("processing get all videos req")
	contents := &models.Contents{
		Data:      []*models.Content{},
		RunningID: webinars.RunningID,
	}
	for _, c := range webinars.Data {
		if c.Status == "active" {
			contents.Data = append(contents.Data, c)
		}
	}
	return operations.NewGetAllWebinarsOK().WithPayload(contents)
}

func (h *Handlers) GetAllContentsHandler(params operations.GetAllContentsParams) middleware.Responder {
	l.Info().Msgf("processing get all contents req")
	contents := &models.Contents{
		Data:      []*models.Content{},
	}
	for _, c := range videos.Data {
		if c.Status == "active" {
			contents.Data = append(contents.Data, c)
		}
	}

	for _, c := range webinars.Data {
		if c.Status == "active" {
			contents.Data = append(contents.Data, c)
		}
	}
	return operations.NewGetAllContentsOK().WithPayload(contents)
}

func (h *Handlers) DeleteWebinarHandler(params operations.DeleteWebinarParams) middleware.Responder {
	l.Info().Msgf("processing delete webinar req for id %v", params.ID)
	foundIndex := 0
	length := len(webinars.Data)
	res := &models.Content{}
	for _, w := range webinars.Data {
		if w.ID == params.ID {
			w.Status = "delete"
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("webinar does not exist %v", params.ID)
	}
	return operations.NewDeleteWebinarOK().WithPayload(res)
}

func (h *Handlers) DeleteVideoHandler(params operations.DeleteVideoParams) middleware.Responder {
	l.Info().Msgf("processing delete video req for id %v", params.ID)
	foundIndex := 0
	length := len(videos.Data)
	res := &models.Content{}
	for _, w := range videos.Data {
		if w.ID == params.ID {
			w.Status = "delete"
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("video does not exist %v", params.ID)
	}
	return operations.NewDeleteVideoOK().WithPayload(res)
}

func (h *Handlers) UpdateVideoHandler(params operations.UpdateVideoParams) middleware.Responder {
	l.Info().Msgf("processing update video req for id %v", params.Body.ID)
	foundIndex := 0
	length := len(videos.Data)
	res := &models.Content{}
	for _, w := range videos.Data {
		if w.ID == params.Body.ID {
			w.Title = params.Body.Title
			w.Link = params.Body.Link
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("video does not exist %v", params.Body.ID)
	}
	return operations.NewUpdateVideoOK().WithPayload(res)
}

func (h *Handlers) UpdateWebinarHandler(params operations.UpdateWebinarParams) middleware.Responder {
	l.Info().Msgf("processing update webinar req for id %v", params.Body.ID)
	foundIndex := 0
	length := len(webinars.Data)
	res := &models.Content{}
	for _, w := range webinars.Data {
		if w.ID == params.Body.ID {
			w.Title = params.Body.Title
			w.Link = params.Body.Link
			res = w
			break
		}
		foundIndex++
	}
	if foundIndex == length {
		l.Info().Msgf("webinar does not exist %v", params.Body.ID)
	}
	return operations.NewUpdateWebinarOK().WithPayload(res)
}

func (h *Handlers) GetVideoByIDHandler(params operations.GetVideoByIDParams) middleware.Responder {
	var content = &models.Content{}
	for _, c := range videos.Data {
		if params.ID == c.ID {
			content = c
			c.Views = c.Views + 1
		}
	}
	return operations.NewGetVideoByIDOK().WithPayload(content)
}

func (h *Handlers) GetWebinarByIDHandler(params operations.GetWebinarByIDParams) middleware.Responder {
	var content = &models.Content{}
	for _, c := range webinars.Data {
		if params.ID == c.ID {
			content = c
			c.Views = c.Views + 1
		}
	}
	return operations.NewGetWebinarByIDOK().WithPayload(content)
}

func (h *Handlers) GetContentsByCourseNameHandler(params operations.GetContentsByCourseNameParams) middleware.Responder {
	url := fmt.Sprintf("http://localhost:8080/subject/course/%v", params.Name)
	var wg sync.WaitGroup
	var m sync.Mutex

	lSubjects := []*models.Subject{}
	lSubjects = parseSubjectsResponse(url)

	lContents := &models.Contents{
		Data: []*models.Content{},
	}
	for _, s := range lSubjects {
		wg.Add(1)
		go func(code string) {
			defer wg.Done()

			for _, v := range videos.Data {
				if v.SubjectCode == code {
					m.Lock()
					lContents.Data = append(lContents.Data, v)
					m.Unlock()
				}
			}

			for _, v := range webinars.Data {
				if v.SubjectCode == code {
					m.Lock()
					lContents.Data = append(lContents.Data, v)
					m.Unlock()
				}
			}

		}(s.Code)
	}
	wg.Wait()
	return operations.NewGetContentsByCourseNameOK().WithPayload(lContents)
}

func (h *Handlers) GetContentsBySubjectNameHandler(params operations.GetContentsBySubjectNameParams) middleware.Responder {
	url := fmt.Sprintf("http://localhost:8080/subject/%v", params.Name)
	subject := parseSubjectResponse(url)

	lContents := &models.Contents{
		Data: []*models.Content{},
	}
	for _, c := range videos.Data {
		if c.SubjectCode == subject.Code {
			lContents.Data = append(lContents.Data, c)
		}
	}

	for _, c := range webinars.Data {
		if c.SubjectCode == subject.Code {
			lContents.Data = append(lContents.Data, c)
		}
	}

	return operations.NewGetContentsBySubjectNameOK().WithPayload(lContents)
}

func (h *Handlers) GetContentsByTagNameHandler(params operations.GetContentsByTagNameParams) middleware.Responder {
	lContents := &models.Contents{
		Data: []*models.Content{},
	}

	var wg sync.WaitGroup
	var m sync.Mutex
	lTag := &models.Tag{}

	for _, t := range tags.Data {
		if t.Name == params.Name {
			lTag.ID = t.ID
			lTag.Code = t.Code
			lTag.Name = t.Name
			break
		}
	}

	for _, c := range videos.Data {
		wg.Add(1)
		go func(ic *models.Content) {
			defer wg.Done()

			for _, t := range ic.Tags {
				if lTag.ID == t {
					m.Lock()
					lContents.Data = append(lContents.Data, ic)
					m.Unlock()
				}
			}

		}(c)
	}

	for _, c := range webinars.Data {
		wg.Add(1)
		go func(ic *models.Content) {
			defer wg.Done()
			for _, t := range ic.Tags {
				if lTag.ID == t {
					m.Lock()
					lContents.Data = append(lContents.Data, ic)
					m.Unlock()
				}
			}

		}(c)
	}
	wg.Wait()
	return operations.NewGetContentsByTagNameOK().WithPayload(lContents)
}

func (h *Handlers) GetMostViewedVideosHandler(params operations.GetMostViewedVideosParams) middleware.Responder {
	mostViewedVideos := buildHeap(int(params.Topn),[]*models.Content{}, ds.NewMostViewedContent(videos.Data))
	lContents := &models.Contents{
		Data: mostViewedVideos,
	}
	return operations.NewGetMostViewedVideosOK().WithPayload(lContents)
}

func (h *Handlers) GetMostViewedWebinarsHandler(params operations.GetMostViewedWebinarsParams) middleware.Responder {
	mostViewedWebinars := buildHeap(int(params.Topn),[]*models.Content{}, ds.NewMostViewedContent(webinars.Data))
	lContents := &models.Contents{
		Data: mostViewedWebinars,
	}
	return operations.NewGetMostViewedWebinarsOK().WithPayload(lContents)
}

func (h *Handlers) GetVideosByTitleHandler(params operations.GetVideosByTitleParams) middleware.Responder {
	trie := ds.Trie{Children: map[byte]ds.Trie{}}
	for _, keyword := range params.Keywords {
		trie.Add(keyword)
	}

	lContents := &models.Contents{
		Data: []*models.Content{},
	}
	for _, v := range videos.Data {
		if trie.ContainsIn(v.Title) {
			lContents.Data = append(lContents.Data, v)
		}
	}
	return operations.NewGetVideosByTitleOK().WithPayload(lContents)
}

func (h *Handlers) GetWebinarsByTitleHandler(params operations.GetWebinarsByTitleParams) middleware.Responder {
	trie := ds.Trie{Children: map[byte]ds.Trie{}}
	for _, keyword := range params.Keywords {
		trie.Add(keyword)
	}

	lContents := &models.Contents{
		Data: []*models.Content{},
	}
	for _, w := range webinars.Data {
		if trie.ContainsIn(w.Title) {
			lContents.Data = append(lContents.Data, w)
		}
	}
	return operations.NewGetWebinarsByTitleOK().WithPayload(lContents)
}

func (h *Handlers) GetWebinarsBySubjectCodeHandler(params operations.GetWebinarsBySubjectCodeParams) middleware.Responder {
	lContents := &models.Contents{
		Data: []*models.Content{},
	}
	for _, w := range webinars.Data {
		if w.SubjectCode == params.Subjectcode {
			lContents.Data = append(lContents.Data, w)
		}
	}
	return operations.NewGetWebinarsBySubjectCodeOK().WithPayload(lContents)
}

func buildHeap(topN int, currentList []*models.Content, videosList *ds.MostViewedContent) []*models.Content {
	if len(currentList) == topN {
		return currentList
	}
	if videosList.Length() <= 1 {
		return append(currentList, videosList.GetContentList()...)
	}
	v := ds.NewMostViewedContent(videosList.GetContentList());
	currentList = append(currentList, v.Remove())
	return buildHeap(topN, currentList, v)
}

func parseSubjectResponse(url string) *models.Subject {
	client := &http.Client{}
	subject := &models.Subject{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		l.Print(err.Error())
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		l.Print(err.Error())
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		l.Print(err.Error())
	}

	json.Unmarshal(bodyBytes, &subject)

	return subject
}

func parseSubjectsResponse(url string) []*models.Subject {
	client := &http.Client{}
	subjects := &models.Subjects{
		Data:      []*models.Subject{},
		RunningID: subjects.RunningID,
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		l.Print(err.Error())
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		l.Print(err.Error())
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		l.Print(err.Error())
	}

	json.Unmarshal(bodyBytes, &subjects)
	return subjects.Data
}
