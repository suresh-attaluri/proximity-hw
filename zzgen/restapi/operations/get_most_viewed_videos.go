// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetMostViewedVideosHandlerFunc turns a function with the right signature into a get most viewed videos handler
type GetMostViewedVideosHandlerFunc func(GetMostViewedVideosParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetMostViewedVideosHandlerFunc) Handle(params GetMostViewedVideosParams) middleware.Responder {
	return fn(params)
}

// GetMostViewedVideosHandler interface for that can handle valid get most viewed videos params
type GetMostViewedVideosHandler interface {
	Handle(GetMostViewedVideosParams) middleware.Responder
}

// NewGetMostViewedVideos creates a new http.Handler for the get most viewed videos operation
func NewGetMostViewedVideos(ctx *middleware.Context, handler GetMostViewedVideosHandler) *GetMostViewedVideos {
	return &GetMostViewedVideos{Context: ctx, Handler: handler}
}

/* GetMostViewedVideos swagger:route GET /video/mostviewed/{topn} getMostViewedVideos

get most viewed videos

*/
type GetMostViewedVideos struct {
	Context *middleware.Context
	Handler GetMostViewedVideosHandler
}

func (o *GetMostViewedVideos) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewGetMostViewedVideosParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
