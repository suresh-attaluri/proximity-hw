// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
)

// CreateVideoOKCode is the HTTP code returned for type CreateVideoOK
const CreateVideoOKCode int = 200

/*CreateVideoOK returns created video details

swagger:response createVideoOK
*/
type CreateVideoOK struct {

	/*uploaded video
	  In: Body
	*/
	Payload *models.Content `json:"body,omitempty"`
}

// NewCreateVideoOK creates CreateVideoOK with default headers values
func NewCreateVideoOK() *CreateVideoOK {

	return &CreateVideoOK{}
}

// WithPayload adds the payload to the create video o k response
func (o *CreateVideoOK) WithPayload(payload *models.Content) *CreateVideoOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create video o k response
func (o *CreateVideoOK) SetPayload(payload *models.Content) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateVideoOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
