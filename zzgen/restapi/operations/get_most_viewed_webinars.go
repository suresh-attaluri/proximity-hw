// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetMostViewedWebinarsHandlerFunc turns a function with the right signature into a get most viewed webinars handler
type GetMostViewedWebinarsHandlerFunc func(GetMostViewedWebinarsParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetMostViewedWebinarsHandlerFunc) Handle(params GetMostViewedWebinarsParams) middleware.Responder {
	return fn(params)
}

// GetMostViewedWebinarsHandler interface for that can handle valid get most viewed webinars params
type GetMostViewedWebinarsHandler interface {
	Handle(GetMostViewedWebinarsParams) middleware.Responder
}

// NewGetMostViewedWebinars creates a new http.Handler for the get most viewed webinars operation
func NewGetMostViewedWebinars(ctx *middleware.Context, handler GetMostViewedWebinarsHandler) *GetMostViewedWebinars {
	return &GetMostViewedWebinars{Context: ctx, Handler: handler}
}

/* GetMostViewedWebinars swagger:route GET /webinar/mostviewed/{topn} getMostViewedWebinars

get most viewed webinars

*/
type GetMostViewedWebinars struct {
	Context *middleware.Context
	Handler GetMostViewedWebinarsHandler
}

func (o *GetMostViewedWebinars) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewGetMostViewedWebinarsParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
