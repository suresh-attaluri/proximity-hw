// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
)

// HealthOKCode is the HTTP code returned for type HealthOK
const HealthOKCode int = 200

/*HealthOK returns health status

swagger:response healthOK
*/
type HealthOK struct {

	/*status up or down
	  In: Body
	*/
	Payload *models.Health `json:"body,omitempty"`
}

// NewHealthOK creates HealthOK with default headers values
func NewHealthOK() *HealthOK {

	return &HealthOK{}
}

// WithPayload adds the payload to the health o k response
func (o *HealthOK) WithPayload(payload *models.Health) *HealthOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the health o k response
func (o *HealthOK) SetPayload(payload *models.Health) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *HealthOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
