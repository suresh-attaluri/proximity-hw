// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
)

// DataloadOKCode is the HTTP code returned for type DataloadOK
const DataloadOKCode int = 200

/*DataloadOK returns health status

swagger:response dataloadOK
*/
type DataloadOK struct {

	/*status up or down
	  In: Body
	*/
	Payload *models.Health `json:"body,omitempty"`
}

// NewDataloadOK creates DataloadOK with default headers values
func NewDataloadOK() *DataloadOK {

	return &DataloadOK{}
}

// WithPayload adds the payload to the dataload o k response
func (o *DataloadOK) WithPayload(payload *models.Health) *DataloadOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the dataload o k response
func (o *DataloadOK) SetPayload(payload *models.Health) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DataloadOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
