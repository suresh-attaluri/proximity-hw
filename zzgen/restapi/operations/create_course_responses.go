// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/models"
)

// CreateCourseOKCode is the HTTP code returned for type CreateCourseOK
const CreateCourseOKCode int = 200

/*CreateCourseOK returns created course details

swagger:response createCourseOK
*/
type CreateCourseOK struct {

	/*created course
	  In: Body
	*/
	Payload *models.Course `json:"body,omitempty"`
}

// NewCreateCourseOK creates CreateCourseOK with default headers values
func NewCreateCourseOK() *CreateCourseOK {

	return &CreateCourseOK{}
}

// WithPayload adds the payload to the create course o k response
func (o *CreateCourseOK) WithPayload(payload *models.Course) *CreateCourseOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create course o k response
func (o *CreateCourseOK) SetPayload(payload *models.Course) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateCourseOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
