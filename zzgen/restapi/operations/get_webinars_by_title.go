// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetWebinarsByTitleHandlerFunc turns a function with the right signature into a get webinars by title handler
type GetWebinarsByTitleHandlerFunc func(GetWebinarsByTitleParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetWebinarsByTitleHandlerFunc) Handle(params GetWebinarsByTitleParams) middleware.Responder {
	return fn(params)
}

// GetWebinarsByTitleHandler interface for that can handle valid get webinars by title params
type GetWebinarsByTitleHandler interface {
	Handle(GetWebinarsByTitleParams) middleware.Responder
}

// NewGetWebinarsByTitle creates a new http.Handler for the get webinars by title operation
func NewGetWebinarsByTitle(ctx *middleware.Context, handler GetWebinarsByTitleHandler) *GetWebinarsByTitle {
	return &GetWebinarsByTitle{Context: ctx, Handler: handler}
}

/* GetWebinarsByTitle swagger:route GET /webinar/title/{keywords} getWebinarsByTitle

getWebinarByTitle

*/
type GetWebinarsByTitle struct {
	Context *middleware.Context
	Handler GetWebinarsByTitleHandler
}

func (o *GetWebinarsByTitle) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewGetWebinarsByTitleParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
