// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetVideoByIDHandlerFunc turns a function with the right signature into a get video by Id handler
type GetVideoByIDHandlerFunc func(GetVideoByIDParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetVideoByIDHandlerFunc) Handle(params GetVideoByIDParams) middleware.Responder {
	return fn(params)
}

// GetVideoByIDHandler interface for that can handle valid get video by Id params
type GetVideoByIDHandler interface {
	Handle(GetVideoByIDParams) middleware.Responder
}

// NewGetVideoByID creates a new http.Handler for the get video by Id operation
func NewGetVideoByID(ctx *middleware.Context, handler GetVideoByIDHandler) *GetVideoByID {
	return &GetVideoByID{Context: ctx, Handler: handler}
}

/* GetVideoByID swagger:route GET /video/id/{id} getVideoById

get video by id

*/
type GetVideoByID struct {
	Context *middleware.Context
	Handler GetVideoByIDHandler
}

func (o *GetVideoByID) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewGetVideoByIDParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
