// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// CreateCourseHandlerFunc turns a function with the right signature into a create course handler
type CreateCourseHandlerFunc func(CreateCourseParams) middleware.Responder

// Handle executing the request and returning a response
func (fn CreateCourseHandlerFunc) Handle(params CreateCourseParams) middleware.Responder {
	return fn(params)
}

// CreateCourseHandler interface for that can handle valid create course params
type CreateCourseHandler interface {
	Handle(CreateCourseParams) middleware.Responder
}

// NewCreateCourse creates a new http.Handler for the create course operation
func NewCreateCourse(ctx *middleware.Context, handler CreateCourseHandler) *CreateCourse {
	return &CreateCourse{Context: ctx, Handler: handler}
}

/* CreateCourse swagger:route POST /course createCourse

create new course

*/
type CreateCourse struct {
	Context *middleware.Context
	Handler CreateCourseHandler
}

func (o *CreateCourse) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewCreateCourseParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
