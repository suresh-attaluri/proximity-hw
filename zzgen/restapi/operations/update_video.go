// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// UpdateVideoHandlerFunc turns a function with the right signature into a update video handler
type UpdateVideoHandlerFunc func(UpdateVideoParams) middleware.Responder

// Handle executing the request and returning a response
func (fn UpdateVideoHandlerFunc) Handle(params UpdateVideoParams) middleware.Responder {
	return fn(params)
}

// UpdateVideoHandler interface for that can handle valid update video params
type UpdateVideoHandler interface {
	Handle(UpdateVideoParams) middleware.Responder
}

// NewUpdateVideo creates a new http.Handler for the update video operation
func NewUpdateVideo(ctx *middleware.Context, handler UpdateVideoHandler) *UpdateVideo {
	return &UpdateVideo{Context: ctx, Handler: handler}
}

/* UpdateVideo swagger:route PUT /video updateVideo

update video

*/
type UpdateVideo struct {
	Context *middleware.Context
	Handler UpdateVideoHandler
}

func (o *UpdateVideo) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewUpdateVideoParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
