// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/suresh-attaluri/proximity-hw/zzgen/restapi/operations"
)

//go:generate swagger generate server --target ../../zzgen --name Proximity --spec ../../swagger.yml --principal interface{} --exclude-main

func configureFlags(api *operations.ProximityAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.ProximityAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()
	api.TxtProducer = runtime.TextProducer()

	if api.CreateCourseHandler == nil {
		api.CreateCourseHandler = operations.CreateCourseHandlerFunc(func(params operations.CreateCourseParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.CreateCourse has not yet been implemented")
		})
	}
	if api.CreateSubjectHandler == nil {
		api.CreateSubjectHandler = operations.CreateSubjectHandlerFunc(func(params operations.CreateSubjectParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.CreateSubject has not yet been implemented")
		})
	}
	if api.CreateTagHandler == nil {
		api.CreateTagHandler = operations.CreateTagHandlerFunc(func(params operations.CreateTagParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.CreateTag has not yet been implemented")
		})
	}
	if api.CreateVideoHandler == nil {
		api.CreateVideoHandler = operations.CreateVideoHandlerFunc(func(params operations.CreateVideoParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.CreateVideo has not yet been implemented")
		})
	}
	if api.CreateWebinarHandler == nil {
		api.CreateWebinarHandler = operations.CreateWebinarHandlerFunc(func(params operations.CreateWebinarParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.CreateWebinar has not yet been implemented")
		})
	}
	if api.DataloadHandler == nil {
		api.DataloadHandler = operations.DataloadHandlerFunc(func(params operations.DataloadParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.Dataload has not yet been implemented")
		})
	}
	if api.DeleteCourseHandler == nil {
		api.DeleteCourseHandler = operations.DeleteCourseHandlerFunc(func(params operations.DeleteCourseParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.DeleteCourse has not yet been implemented")
		})
	}
	if api.DeleteSubjectHandler == nil {
		api.DeleteSubjectHandler = operations.DeleteSubjectHandlerFunc(func(params operations.DeleteSubjectParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.DeleteSubject has not yet been implemented")
		})
	}
	if api.DeleteTagHandler == nil {
		api.DeleteTagHandler = operations.DeleteTagHandlerFunc(func(params operations.DeleteTagParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.DeleteTag has not yet been implemented")
		})
	}
	if api.DeleteVideoHandler == nil {
		api.DeleteVideoHandler = operations.DeleteVideoHandlerFunc(func(params operations.DeleteVideoParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.DeleteVideo has not yet been implemented")
		})
	}
	if api.DeleteWebinarHandler == nil {
		api.DeleteWebinarHandler = operations.DeleteWebinarHandlerFunc(func(params operations.DeleteWebinarParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.DeleteWebinar has not yet been implemented")
		})
	}
	if api.GetAllContentsHandler == nil {
		api.GetAllContentsHandler = operations.GetAllContentsHandlerFunc(func(params operations.GetAllContentsParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllContents has not yet been implemented")
		})
	}
	if api.GetAllCoursesHandler == nil {
		api.GetAllCoursesHandler = operations.GetAllCoursesHandlerFunc(func(params operations.GetAllCoursesParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllCourses has not yet been implemented")
		})
	}
	if api.GetAllSubjectsHandler == nil {
		api.GetAllSubjectsHandler = operations.GetAllSubjectsHandlerFunc(func(params operations.GetAllSubjectsParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllSubjects has not yet been implemented")
		})
	}
	if api.GetAllTagsHandler == nil {
		api.GetAllTagsHandler = operations.GetAllTagsHandlerFunc(func(params operations.GetAllTagsParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllTags has not yet been implemented")
		})
	}
	if api.GetAllVideosHandler == nil {
		api.GetAllVideosHandler = operations.GetAllVideosHandlerFunc(func(params operations.GetAllVideosParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllVideos has not yet been implemented")
		})
	}
	if api.GetAllWebinarsHandler == nil {
		api.GetAllWebinarsHandler = operations.GetAllWebinarsHandlerFunc(func(params operations.GetAllWebinarsParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetAllWebinars has not yet been implemented")
		})
	}
	if api.GetContentsByCourseNameHandler == nil {
		api.GetContentsByCourseNameHandler = operations.GetContentsByCourseNameHandlerFunc(func(params operations.GetContentsByCourseNameParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetContentsByCourseName has not yet been implemented")
		})
	}
	if api.GetContentsBySubjectNameHandler == nil {
		api.GetContentsBySubjectNameHandler = operations.GetContentsBySubjectNameHandlerFunc(func(params operations.GetContentsBySubjectNameParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetContentsBySubjectName has not yet been implemented")
		})
	}
	if api.GetContentsByTagNameHandler == nil {
		api.GetContentsByTagNameHandler = operations.GetContentsByTagNameHandlerFunc(func(params operations.GetContentsByTagNameParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetContentsByTagName has not yet been implemented")
		})
	}
	if api.GetCoursesBySubjectCodeHandler == nil {
		api.GetCoursesBySubjectCodeHandler = operations.GetCoursesBySubjectCodeHandlerFunc(func(params operations.GetCoursesBySubjectCodeParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetCoursesBySubjectCode has not yet been implemented")
		})
	}
	if api.GetMostViewedVideosHandler == nil {
		api.GetMostViewedVideosHandler = operations.GetMostViewedVideosHandlerFunc(func(params operations.GetMostViewedVideosParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetMostViewedVideos has not yet been implemented")
		})
	}
	if api.GetMostViewedWebinarsHandler == nil {
		api.GetMostViewedWebinarsHandler = operations.GetMostViewedWebinarsHandlerFunc(func(params operations.GetMostViewedWebinarsParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetMostViewedWebinars has not yet been implemented")
		})
	}
	if api.GetSubjectByNameHandler == nil {
		api.GetSubjectByNameHandler = operations.GetSubjectByNameHandlerFunc(func(params operations.GetSubjectByNameParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetSubjectByName has not yet been implemented")
		})
	}
	if api.GetSubjectsByCourseNameHandler == nil {
		api.GetSubjectsByCourseNameHandler = operations.GetSubjectsByCourseNameHandlerFunc(func(params operations.GetSubjectsByCourseNameParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetSubjectsByCourseName has not yet been implemented")
		})
	}
	if api.GetVideoByIDHandler == nil {
		api.GetVideoByIDHandler = operations.GetVideoByIDHandlerFunc(func(params operations.GetVideoByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetVideoByID has not yet been implemented")
		})
	}
	if api.GetVideosByTitleHandler == nil {
		api.GetVideosByTitleHandler = operations.GetVideosByTitleHandlerFunc(func(params operations.GetVideosByTitleParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetVideosByTitle has not yet been implemented")
		})
	}
	if api.GetWebinarByIDHandler == nil {
		api.GetWebinarByIDHandler = operations.GetWebinarByIDHandlerFunc(func(params operations.GetWebinarByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetWebinarByID has not yet been implemented")
		})
	}
	if api.GetWebinarsBySubjectCodeHandler == nil {
		api.GetWebinarsBySubjectCodeHandler = operations.GetWebinarsBySubjectCodeHandlerFunc(func(params operations.GetWebinarsBySubjectCodeParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetWebinarsBySubjectCode has not yet been implemented")
		})
	}
	if api.GetWebinarsByTitleHandler == nil {
		api.GetWebinarsByTitleHandler = operations.GetWebinarsByTitleHandlerFunc(func(params operations.GetWebinarsByTitleParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetWebinarsByTitle has not yet been implemented")
		})
	}
	if api.HealthHandler == nil {
		api.HealthHandler = operations.HealthHandlerFunc(func(params operations.HealthParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.Health has not yet been implemented")
		})
	}
	if api.RootHandler == nil {
		api.RootHandler = operations.RootHandlerFunc(func(params operations.RootParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.Root has not yet been implemented")
		})
	}
	if api.UpdateCourseHandler == nil {
		api.UpdateCourseHandler = operations.UpdateCourseHandlerFunc(func(params operations.UpdateCourseParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.UpdateCourse has not yet been implemented")
		})
	}
	if api.UpdateSubjectHandler == nil {
		api.UpdateSubjectHandler = operations.UpdateSubjectHandlerFunc(func(params operations.UpdateSubjectParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.UpdateSubject has not yet been implemented")
		})
	}
	if api.UpdateTagHandler == nil {
		api.UpdateTagHandler = operations.UpdateTagHandlerFunc(func(params operations.UpdateTagParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.UpdateTag has not yet been implemented")
		})
	}
	if api.UpdateVideoHandler == nil {
		api.UpdateVideoHandler = operations.UpdateVideoHandlerFunc(func(params operations.UpdateVideoParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.UpdateVideo has not yet been implemented")
		})
	}
	if api.UpdateWebinarHandler == nil {
		api.UpdateWebinarHandler = operations.UpdateWebinarHandlerFunc(func(params operations.UpdateWebinarParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.UpdateWebinar has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
