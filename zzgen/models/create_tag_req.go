// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// CreateTagReq create tag req
//
// swagger:model createTagReq
type CreateTagReq struct {

	// code
	Code string `json:"code,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this create tag req
func (m *CreateTagReq) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this create tag req based on context it is used
func (m *CreateTagReq) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *CreateTagReq) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *CreateTagReq) UnmarshalBinary(b []byte) error {
	var res CreateTagReq
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
